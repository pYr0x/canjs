import "steal-mocha";

describe("Foo", function(){
  it("bar", function(){
    assert.isBelow(3, 6, '3 is strictly less than 6');
    assert("yay!");
  });
});

// describe("file", function () {
//   it("exists", function (done) {
//     this.timeout(5000);
//
//     fetch('../dist/foo.md')
//       .then(res => {
//         if(res.ok) {
//           return res.blob();
//         }
//         throw new Error('file not found');
//       })
//       .then(blob => {
//         assert.ok("file found");
//         done();
//       })
//       .catch(error => {
//         done(error);
//       });
//   });
// });