const testee = require('testee');
const config = require("./testee-config");

const browsers = 'chromium';
const files = ['test/test.html'];

testee.test(files, browsers, config)
      .then(function() {
        console.log('done!')
      });