const puppeteer = require('puppeteer');
process.env.LAUNCHPAD_CHROMIUM = puppeteer.executablePath();

module.exports = {
  "browsers": [
    // "firefox"
    // "chrome"
    {
      "browser": "chromium",
      "args": [
        "--headless",
        "--disable-gpu",
        "--no-sandbox",
        "--disable-setuid-sandbox",
        "--remote-debugging-port=9222"
      ]
    }
  ],
  "reporter": "spec"
};